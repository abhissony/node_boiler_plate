import express from 'express';
import user from '../app/controllers/publicApi.js';
let router = express.Router();

/* GET test. */
router.get('/', function(req, res, next) {
    res.send('respond with a resource');
});

/* User Sign Up */
router.post('/signup', user.signUp)

/* User Log In */
router.post('/login', user.logIn)

/* User Log Out */
router.get('/logout', user.logOut)

/* forgotPassword */
router.post('/forgotPassword', user.forgotPassword)

/* resetPassword */
router.post('/resetPassword/:token', user.resetPassword)

export default router;