import express from 'express';
import user from '../app/controllers/userCtrl'
var router = express.Router();

/* GET test. */
router.get('/', function (req, res) {
    res.send({ title: 'user' });
});

/* For Profile CRUD */
router.route('/profile')
    .get(user.getProfile)
    .post(user.setProfile)
    .put(user.setProfile)

export default router;