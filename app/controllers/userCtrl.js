import CONSTANTS from '../../util/constants'
import User from '../models/users';
import LogIn from '../models/loginSchema';
import QUERYCTRL from './queryCtrl'
import UTILITY from '../../util/utilityMethod'

/*________________________________________________________________________
 * @Date        :   
 * Modified by  :	
 * @Author      :   Abhishek verma
 * @Purpose     :   This function is used for Profile CRUD.
 _________________________________________________________________________
*/

const getProfile = async (req,res) => {
    let query = { _id: res.locals.decodedToken._id },
        projection = { __v: 0, password: 0, status:0}
    let response = await QUERYCTRL.getData(req, res, User, query, projection)
    if (response){
        res.send(CONSTANTS.getResponseMessage(response))
    }
}

const setProfile = async (req, res) => {
    // if (UTILITY.checkRequiredParams(req.body, res, ['dob', 'height', 'weight', 'gender', 'fitness_level', 'fitness_goal_id', 'body_fat', 'meal_preferences', 'allergies_id', 'allergies_text'])) {
    if (UTILITY.checkRequiredParams(req.body, res, ['dob'])) {
        let query = { _id: res.locals.decodedToken._id },
            reqData = { $set: { 'profile': req.body, is_profile_setup: true } },
            response = await QUERYCTRL.updateData(reqData, res, User, query)
        if (response) {
            let { user_name, email } = response,
            data = [
                { id: response._id, user_name, email, profile:response.profile }
            ]
            res.send(CONSTANTS.getResponseMessage(data))
        }
    }
}

let userCtrl = {
    getProfile,
    setProfile
};
export default userCtrl;
