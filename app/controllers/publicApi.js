import bcrypt from 'bcrypt';
import crypto from 'crypto';
import CONSTANTS from '../../util/constants'
import User from '../models/users';
import LogIn from '../models/loginSchema';
import QUERYCTRL from './queryCtrl'
import UTILITY from '../../util/utilityMethod'
import Mail from '../../util/sendMail'

/*________________________________________________________________________
 * @Date        :   
 * Modified by  :	
 * @Author      :   Alogoworks
 * @Purpose     :   This function is used for Sign Up.
 _________________________________________________________________________
*/
const signUp = async (req,res) => {
    let { user_name, email, password } = req.body,
        reqBody         = { user_name, email, password },
        signUpResponse  = await QUERYCTRL.saveData(reqBody, res, User),
        token           = await UTILITY.getJwtToken({ email: signUpResponse.email, _id:signUpResponse._id}),
        logInResponse   = await QUERYCTRL.saveData({ userId: signUpResponse._id, token: token}, res, LogIn)
    if (logInResponse) {
        let { user_name, email, is_profile_setup } = signUpResponse,
        data = [
            { id: signUpResponse._id, user_name, email, is_profile_setup, access_token: logInResponse.token}
        ]
        res.send(CONSTANTS.getResponseMessage(data))
    }
}

/*________________________________________________________________________
 * @Date        :   
 * Modified by  :	
 * @Author      :   Alogoworks
 * @Purpose     :   This function is used for Log In.
 _________________________________________________________________________
*/
const logIn = async (req, res) => {
    if (UTILITY.checkRequiredParams(req.body, res, ['email','password'])){
        let query = { email: req.body.email },
            response = await QUERYCTRL.getData(req, res, User, query)
        if (response[0] && response[0].status) {
            let result = await bcrypt.compare(req.body.password, response[0].password);
            if (result) {
                let token = await UTILITY.getJwtToken({ email: response[0].email, _id: response[0]._id }),
                    logInResponse = await QUERYCTRL.saveData({ userId: response[0]._id, token: token }, res, LogIn);
                if (logInResponse) {
                    let { user_name, email, is_profile_setup } = response[0],
                        data = [
                            { id: response[0]._id, user_name, email, is_profile_setup, access_token: logInResponse.token, profile:response[0].profile }
                        ]
                    return res.send(CONSTANTS.getResponseMessage(data))
                }
            }
            else
                return res.send(CONSTANTS.getCustomErrorMessage("password", CONSTANTS.LOGIN.INVALID_PASSWORD))
        }
        else if (response[0] && !response[0].status)
            return res.send(CONSTANTS.getCustomErrorMessage("email", CONSTANTS.LOGIN.DEACTIVATED))
        else
            return res.send(CONSTANTS.getCustomErrorMessage("email", CONSTANTS.LOGIN.INVALID_EMAIL))
    }
}

/*________________________________________________________________________
 * @Date        :   
 * Modified by  :	
 * @Author      :   Alogoworks
 * @Purpose     :   This function is used for Log Out.
 _________________________________________________________________________
*/
const logOut = async (req, res) => {
    if (UTILITY.checkRequiredParams(req.headers, res, ['authorization'])) {
        let query = { token: req.headers.authorization},
            response = await QUERYCTRL.deleteData(req, res, LogIn, query)
        if(response){
            let message = (response.result.n > 0) ? LOGOUT.SUCCESS : LOGOUT.NOT_FOUND
            return res.send(CONSTANTS.getResponseMessage([], false, message))
        }
    }
}

/*________________________________________________________________________
 * @Date        :   
 * Modified by  :	
 * @Author      :   Alogoworks
 * @Purpose     :   This function is used for send the password reset link.
 _________________________________________________________________________
*/
var forgotPassword = async (req, res) => {
    if(UTILITY.checkRequiredParams(req.body,res,['email'])){
        let query       = { email: req.body.email },
            response    = await QUERYCTRL.getData(req, res, User, query)
        if (response.length) {
            let buf     = await crypto.randomBytes(5),
                token   = buf.toString('hex'),
                reqData = { resetPasswordToken: token, resetPasswordExpires: CONSTANTS.DATA.RESET_PASSWORD_EXPIRY },
                saveResponse  = await QUERYCTRL.updateData(reqData, res, User, query)
            if (saveResponse) {
                let data = [
                    { email : saveResponse.email }
                ]
                res.send(CONSTANTS.getResponseMessage(data, false, CONSTANTS.DATA.MAIL_SENT))
                Mail.resetPwdMail(response[0], token);
            }
        }
        else{
            let errObj = {
                error_message   : CONSTANTS.DATA.INVALID_EMAIL,
                error_code      : CONSTANTS.ERROR_CODE.INVALID,
                error           : {
                    email: CONSTANTS.DATA.INVALID_EMAIL
                },
            }
            res.send(CONSTANTS.getResponseMessage([], errObj, errObj.error_message))
        }
    }
}

/*________________________________________________________________________
 * @Date        :   
 * Modified On  :	
 * @Author      :   Alogoworks
 * @Purpose     :   This function is used  to reset password.
 _________________________________________________________________________
*/
const resetPassword = async (req, res) => {
    let query = { resetPasswordToken: req.params.token },
        { new_password, confirm_password } = req.body;
    if (UTILITY.checkRequiredParams(req.body, res, ['new_password','confirm_password'])) {
        let response = await QUERYCTRL.getData(req, res, User, query);
        if (response.length) {
            if (new_password == confirm_password) {
                let reqBody = {}
                reqBody.password = await UTILITY.encryptPassword(new_password)
                reqBody.resetPasswordToken = "";
                let data = await QUERYCTRL.updateData(reqBody, res, User, query)
                if (data) {
                    let logOutResponce = await QUERYCTRL.deleteData(req, res, LogIn, { userId: response[0]._id })
                    if(logOutResponce)
                        res.send(CONSTANTS.getResponseMessage([], false, CONSTANTS.DATA.PASSWORD_SUCCESS))
                }
            }
            else
                return res.send(CONSTANTS.getCustomErrorMessage("password", CONSTANTS.DATA.PASSWORD_MISMATCHED))
        } 
        else
            return res.send(CONSTANTS.getCustomErrorMessage("url", CONSTANTS.DATA.PASSWORD_EXPIRED))
    } 
}

let publicApi = {
    signUp,
    logIn,
    logOut,
    forgotPassword,
    resetPassword
};
export default publicApi;
