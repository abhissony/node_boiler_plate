import CONSTANTS from '../../util/constants'
import errHandler from '../../util/errHandler'
import bcrypt from 'bcrypt';
import Login from '../models/loginSchema'

const getData = async (req, res, MODEL, query, projection) => {
    try {
        let response = await MODEL.find(query, projection)
        return response
    }
    catch (error) {
        res.send(errHandler(error))
        return false
    }
}

const saveData = async (reqBody, res, MODEL) => {
    try {
        let response = await MODEL(reqBody).save()
        return response
    }
    catch (error) {
        res.send(errHandler(error))
        return false
    }
}

const updateData = async (reqBody, res, MODEL, query) => {
    try {
        let response = await MODEL.findOneAndUpdate(query, reqBody, { new: true })
        return response
    }
    catch (error) {
        res.send(errHandler(error))
        return false
    }
}

const deleteData = async (req, res, MODEL, query) => {
    try {
        let response = await MODEL.remove(query)
        return response
    }
    catch (error) {
        res.send(errHandler(error))
        return false
    }
}

let queryCtrl = {
    getData,
    saveData,
    updateData,
    deleteData
}

export default queryCtrl;