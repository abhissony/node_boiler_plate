import mongoose from 'mongoose';
import bcrypt from 'bcrypt';
import uniqueValidator from 'mongoose-unique-validator';
import CONSTANTS from '../../util/constants'

/*________________________________________________________________________
 * @Date        : 
 * Modified On  :	
 * @Author      : Abhishek verma
 * @Purpose     : This is the schema of User
 _________________________________________________________________________
*/
let Schema = mongoose.Schema;
let userSchema = new Schema({
  user_name           : { type  : String, required  : true },
  email               : { type  : String, required  : true, unique: false },
  password            : { type  : String, required  : true },
  is_profile_setup    : { type  : Boolean, default  : false },
  status              : { type  : Boolean, default  : true },
  profile             : {
                          dob             : { type: String },
                          height          : { type: String },
                          weight          : { type: String },
                          gender          : { type: String }, 
                          fitness_level   : { type: Number },
                          fitness_goal_id : { type: Number },
                          body_fat        : { type: String },
                          meal_preferences: { type: Number },
                          allergies_id    : { type: Number },
                          allergies_text  : { type: String }
                        },

  resetPasswordToken  : { type  : String },
  resetPasswordExpires: { type  : Date },
  isDeleted           : { type  : Boolean, default: false, select: false },
  created_at          : { type  : Date, default : Date.now, select: false },
  updated_at          : { type  : Date, default : Date.now, select: false }
});

userSchema.pre('save', function (next) {
  let user = this;
  if (!user.isModified('password')) return next();
  bcrypt.genSalt(CONSTANTS.SALT_WORK_FACTOR, function (err, salt) {
    if (err) return next(err);
    bcrypt.hash(user.password, salt, function (err, hash) {
      if (err) return next(err);
      user.password = hash;
      next();
    });
  });
});

// userSchema.pre('update', function(next) {
//   let user = this;

//   this.update({},{ $set: { updatedAt: new Date() } });
// });

userSchema.plugin(uniqueValidator);
userSchema.plugin(uniqueValidator, { message: "Email already exists." });
let users = mongoose.model('users', userSchema);
export default users;