module.exports = {
    development: {
        db  : 'mongodb://localhost:27017/node_sample',
        port: process.env.PORT || 3000,
        env : 'development'
    },
    production: {
        db  : 'mongodb://localhost:27017/node_sample',        
        port: process.env.PORT || 4000,
        env : 'production'
    }
}