import express from 'express';
import AUTHENTICATION from '../util/authentication'
import index from '../routes/index';
import publicApi from '../routes/publicApi';
import userRoute from '../routes/userRoute';

module.exports = (app) => {
    let router = express.Router();
    router.use((req, res, next) => {
        AUTHENTICATION(req, res, next)
    });
    
    app.get('/hello', (req, res) => {
        res.send("Hi to " + req.device.type.toUpperCase() + " User");
    });
    
    app.use('/', index);
    app.use('/publicApi', publicApi);
    app.use('/user', router, userRoute);
}