import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt'

import CONSTANTS from './constants'
import errHandler from './errHandler'

const getJwtToken = async (data) => {
    let token = await jwt.sign(
        {
            data: data,
            exp : CONSTANTS.JWT.EXPIRY_TIME
        },
        CONSTANTS.JWT.SECRET_KEY);
    return token
}

const verifyJwtToken = async (req, res) => {
    try{
        let decoded = await jwt.verify(req.headers.authorization, CONSTANTS.JWT.SECRET_KEY)
        return decoded
    }
    catch (error) {
        res.send(errHandler(error))
        return false
    }
}

const checkRequiredParams = (data, res, requiredParams) => {
    let fields = Object.keys(data),
        check = [];

    for (let param of requiredParams) {
        let val = (fields.includes(param) && (data[param] != ""))
        if (!val)
            check.push(param)
    }
    
    if (check.length) {
        res.send(errHandler({ name: 'customRequiredCheck', values: check }));
        return false;
    }
    else
        return true;
}

const encryptPassword = async (data) => {
    let salt = await bcrypt.genSalt(CONSTANTS.SALT_WORK_FACTOR)
    if (salt) {
        let hash = await bcrypt.hash(data, salt)
        if (hash) {
            return hash
        }
        else
            return err
    }
    else
        return err
}

let utilityMethods = {
    getJwtToken,
    verifyJwtToken,
    checkRequiredParams,
    encryptPassword
};

export default utilityMethods;