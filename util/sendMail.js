import nodemailer from 'nodemailer';
import path from 'path';
import pug from 'pug';
import CONSTANT from './constants';

let transporter = nodemailer.createTransport();

const Mail = {
    resetPwdMail: async (req, token) => {
        let obj = {};
        obj.msg = pug.renderFile(path.join(__dirname, '../views/forgotPasswordEmail.pug'), { resetPasswordUrl: CONSTANT.BASE_PATH.RESET_PASSWORD_PATH + token, name: req.user_name });
        obj.subject = 'Reset Password';
        obj.email = req.email;
        Mail.send(obj);
    },
    send: async (obj) => {
        let data = {
            from: CONSTANT.GMAIL_SMTP_CREDENTIAL.USERNAME,
            to: obj.email,
            subject: (obj.subject || "No Subject"),
            html: obj.msg
        };
        transporter.sendMail(data, (error, res) => {
            if (error) {
                console.log("error", error);
                return false
            } 
            else{
                console.log('Email sent: ' + res.messageId);
                return true;
            }
        })
    }
};

export default Mail;