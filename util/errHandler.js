import CONSTANTS from '../util/constants'
import HttpStatus from 'http-status-codes'

const errMessage = function (err) {
    let errMsg = {},
        obj = {};
    errMsg['error_message'] = CONSTANTS.FILLALLFIELDS;
    if (err.name == 'ValidationError') {
        for (let field in err.errors) {
            let key = err.errors[field].path;
            let val;
            if (err.errors[field].properties && err.errors[field].properties.type){
                if (err.errors[field].properties.type == 'required'){
                    val = key.replace('_', ' ')                    
                    val = val.charAt(0).toUpperCase() + val.slice(1) + ' is ' + err.errors[field].kind + '.';
                }
                else if (err.errors[field].properties.type == 'unique') {
                    val = key.replace('_', ' ')
                    val = key.charAt(0).toUpperCase() + val.slice(1) + ' already exists.';
                    errMsg['error_message'] = val;
                }
                else{
                    val = err.errors[field].message;
                }
            }
            else if (err.errors[field].kind == "ObjectID"){
                val = 'Invalid ' + key + ' id.';
            }    
            else 
                val = err.errors[field].message;
            obj[key] = val;
        }
    }
    else if( err.name == 'customRequiredCheck'){
        for( let i = 0; i < err.values.length ; i++) {
            let val = err.values[i].charAt(0).toUpperCase() + err.values[i].slice(1) + ' is required.';
            val = val.replace('_', ' ')
            obj[err.values[i]] = val;
        }
    }
    else if (err.name == 'passwordValidationError')
        obj = { password : 'Password is not valid.'}; 
    else if (err.name == 'MongoError'){
        let errMessage = err.message
        if (errMessage.includes("dup key")){
            errMessage = errMessage.substring(errMessage.indexOf('$') + 1);
            let key = errMessage.substring(0, errMessage.indexOf(' ') - 2);
            let val = (key.charAt(0).toUpperCase() + key.slice(1)).replace('_', ' ');
            errMsg['error_message'] = val + ' has been already taken.';
            obj[key] = val + ' has been already taken.'
        }
        else{
            obj = err;
        }
    }
    else if (err.name == 'CastError' && err.path == '_id') 
        obj[err.path] = 'Invalid id.';
    else if (err.name == 'JsonWebTokenError'){
        errMsg['error_message'] = err.message
        obj = { authorization: err.message}
    }
    else
        obj = err;

    errMsg['error_code'] = CONSTANTS.ERROR_CODE.INVALID;
    errMsg['error'] = obj;
    return CONSTANTS.getResponseMessage([], errMsg, errMsg.error_message);
}

export default errMessage