import QUERYCTRL from '../app/controllers/queryCtrl'
import Login from '../app/models/loginSchema';
import UTILITY from './utilityMethod'

module.exports = async (req, res, next) => {
    if (UTILITY.checkRequiredParams(req.headers, res, ['authorization'])) {
        let query = { token: req.headers.authorization}
        let response = await QUERYCTRL.getData(req, res, Login, query)
        if (response){
            let decoded = await UTILITY.verifyJwtToken(req, res)
            if(decoded){
                res.locals.decodedToken = decoded.data;
                next()
            }
        }
    }
}