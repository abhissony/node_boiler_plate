import HttpStatus from 'http-status-codes'

const FILLALLFIELDS =  "Please Provide All Details.";
const BASE_PATH = {
    'RESET_PASSWORD_PATH' : "reset-password/"
}

const DATA = {
    "RESET_PASSWORD_EXPIRY": Date.now() + 3600000,
    "INVALID_EMAIL": "Email not found.",
    "MAIL_SENT": "Email sent successfully.",
    "PASSWORD_REQUIRED": "Please provide all valid details.",
    "PASSWORD_MISMATCHED": "Confirm password mismatched.",
    "PASSWORD_EXPIRED": "Invalid URL.",
    "PASSWORD_SUCCESS": "Password has been successfully updated."
}

const JWT = {
    "SECRET_KEY": "aNEWu_abhishek",
    "EXPIRY_TIME": Date.now() + (60 * 60 * 1000)
}

const ERROR_CODE = {
    "INVALID": HttpStatus.NOT_FOUND
}

const LOGIN = {
    "INVALID_PASSWORD"  : "Password is invalid.",
    "INVALID_EMAIL"     : "Email not found.",
    "DEACTIVATED"       : "Your account is currently deactivated."
}

const LOGOUT = {
    "SUCCESS"   : "Logout successful.",
    "NOT_FOUND" : "Login token not found.",
}

const GMAIL_SMTP_CREDENTIAL = {
    "USERNAME": "abhishekverma.algoworks@gmail.com"
}

const getResponseMessage = (data, error, message) => {
    return { status: error ? false : true, message: message, data: data, error: error ? error:{} }
}

const getCustomErrorMessage = (type,message) => {
    let errMsg = {
        error_message: message,
        error_code: ERROR_CODE.INVALID,
        error: {
            [type]: message
        }
    }
    return getResponseMessage([], errMsg, message)
}

var constants = {
    FILLALLFIELDS,
    BASE_PATH,
    DATA,
    JWT,
    ERROR_CODE,
    LOGIN,
    GMAIL_SMTP_CREDENTIAL,
    getResponseMessage,
    getCustomErrorMessage
};

module.exports = constants;